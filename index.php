<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/11/18
 * Time: 1:06 PM
 */

ini_set('max_execution_time', -1);
ini_set('xdebug.var_display_max_depth', 5);

require_once __DIR__ . '/vendor/autoload.php';

define('ROOT', __DIR__);

$path = ROOT . '/data/uncompressed.eu4';

$parser = new \Eu4Parser\Parsers\Eu4TxtParser($path);

try {
    $results = $parser->process();

    var_dump($results);
}
catch(Exception $e) {}