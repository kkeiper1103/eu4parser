<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/16/18
 * Time: 12:23 PM
 */

namespace Eu4Parser;


use Closure;
use Eu4Parser\Loaders\TextFileLoader;
use Eu4Parser\Loaders\ZipLoader;

class Parser
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var \SplTempFileObject
     */
    protected $data;

    /**
     * @var array<callable>
     */
    protected $handlers = [];

    /**
     * @var \Adbar\Dot|array
     */
    protected $tree;

    /**
     * @var array
     */
    protected $keys = [];

    /**
     * @var int unsigned
     */
    protected $depth = 0;

    /**
     * Parser constructor.
     * @param $file string path to file
     */
    public function __construct($file)
    {
        $this->path = $file;
        $this->data = $this->load($file);

        $this->tree = dot([]);
    }

    /**
     * @param $file
     * @return \SplTempFileObject
     */
    private function load($file) {
        switch(mime_content_type($file)) {
            case "application/zip":
                $loader = new ZipLoader();
                break;

            default:
                $loader = new TextFileLoader();
                break;
        }

        /**
         * Move loaded text into a temp file for generator
         */
        $tmpfile = new \SplTempFileObject();
        $tmpfile->fwrite($loader->load($file));
        $tmpfile->rewind();

        unset($loader);

        return $tmpfile;
    }

    /**
     * @return \Generator
     */
    protected function getCharacters()
    {
        while(!$this->data->eof()) {
            yield $this->data->fgetc();
        }
    }

    /**
     *
     */
    public function process()
    {
        $buffer = '';

        foreach($this->getCharacters() as $char) {
            // if the character has a handler
            if (in_array($char, array_keys($this->handlers))) {
                // run the handler on the buffer
                $retval = $this->handlers[$char](trim($buffer));

                // clear buffer
                $buffer = '';
            } // otherwise, just append char to the buffer
            else {
                $buffer .= $char;
            }
        }

        return $this->tree->all();
    }

    /**
     * @param array $handlers
     */
    public function setHandlers(array $handlers) {
        foreach($handlers as $char => $fn) {
            $this->setHandler($char, $fn);
        }
    }

    /**
     * @param $character
     * @param callable $callback
     */
    protected function setHandler($character, callable $callback) {
        $this->handlers[$character] = Closure::bind($callback, $this, Parser::class);
    }
}