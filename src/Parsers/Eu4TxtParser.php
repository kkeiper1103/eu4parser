<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/18/18
 * Time: 8:22 AM
 */

namespace Eu4Parser\Parsers;


use Eu4Parser\Parser;

class Eu4TxtParser extends Parser
{
    public function __construct($file)
    {
        parent::__construct($file);

        /**
         *
         */
        $this->setHandlers([
            // ='s sign means we need to set the key
            '=' => function($buffer) {
                $this->keys[$this->depth] = $buffer;
            },

            // we're entering a sub object; increase depth
            '{' => function($buffer) {
                if (is_int($this->keys[$this->depth])) {
                    $this->keys[$this->depth]++;
                } else if( empty($this->keys[$this->depth]) ) {
                    $this->keys[$this->depth] = 0;
                }

                $this->depth++;
            },

            // we're leaving a sub object; decrease depth
            '}' => function($buffer) {
                // unset key for depth
                unset($this->keys[$this->depth]);

                $this->depth--;
            },

            // a new line means it's time for an assignment
            "\n" => function($buffer) {
                // skip processing if A) no buffer or B) is comment / EU4txt
                if($buffer == '' || empty($this->keys)) return;

                // build path to array by concatenating depth keys
                // etc keys = ["trade", "nodes", "KAT"]
                // becomes -> "trade.nodes.KAT"

                $path = implode('.', $this->keys);

                // if the key already exists, grab the existing item
                if($this->tree->has($path)) {
                    $tmp = $this->tree->get($path);

                    // if it's not an array, make it one
                    if(!is_array($tmp)) $tmp = [$tmp];

                    // append buffer to the existing item
                    $tmp[] = $buffer;

                    // and set buffer to tmp, for storage next
                    $buffer = $tmp;
                }


                $this->tree->set($path, $buffer);
            }
        ]);
    }
}