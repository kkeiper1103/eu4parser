<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/17/18
 * Time: 2:25 PM
 */

namespace Eu4Parser\Loaders;


interface iLoader
{
    /**
     * @param $path string Path to file to load
     * @return string
     */
    public function load($path);
}