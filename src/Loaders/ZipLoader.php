<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/17/18
 * Time: 2:26 PM
 */

namespace Eu4Parser\Loaders;


use ZipArchive;

class ZipLoader implements iLoader
{

    /**
     * @param $path string Path to file to load
     * @return string
     */
    public function load($path)
    {
        $archive = new ZipArchive();
        $archive->open($path);

        // initialize the 3 files, then concat
        list(
            $ai,
            $meta,
            $gamestate
            ) = [
            $archive->getFromName("ai"),
            $archive->getFromName("meta"),
            $archive->getFromName("gamestate")
        ];

        return implode(PHP_EOL, [
            $meta, $gamestate, $ai
        ]);
    }
}