<?php
/**
 * Created by PhpStorm.
 * User: kkeiper1103
 * Date: 4/17/18
 * Time: 2:26 PM
 */

namespace Eu4Parser\Loaders;


class TextFileLoader implements iLoader
{

    /**
     * @param $path string Path to file to load
     * @return string
     */
    public function load($path)
    {
        return file_get_contents($path);
    }
}